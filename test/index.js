const express = require('express');
const app = express();
const fs = require('fs');
const path = require('path');

app.use(express.static('../assets'));
//app.use(express.static('../.public/views'));

app.listen(80, () => {
    console.log('welp');
});


const templates = fs.readdirSync('../templates')
    .filter(a => a.endsWith('.template.html')).map(a => path.parse(a).name.replace(/.template/g, ''));
const regex = /{{(\w+)}}/g;

if (!fs.existsSync('../.public'))
    fs.mkdirSync('../.public');

const {readdir} = fs.promises;
const {resolve} = path;
    
async function* getFiles(dir) {
    const dirents = await readdir(dir, { withFileTypes: true });

    for (const dirent of dirents) {
        const res = resolve(dir, dirent.name);
        if (dirent.isDirectory()) {
            yield* getFiles(res);
        } else {
            yield res;
        }
    }
}

app.get(`/*`, (req, res) => {
    const file = path.resolve(`../views/${req.path}/${req.path.endsWith('.html') ? '' : 'index.html'}`);

    if (!fs.existsSync(file)) {
        res.status(404).send(buildTemplate(path.resolve('../views/404.html')));
        return;
    }

    res.send(buildTemplate(file));
});

function buildTemplate(view) {
    const viewData = fs.readFileSync(view, 'utf8');

    if (!regex.test(viewData))
        return viewData;

    let result = viewData;
    let addedTemplates = [];

    for (const template of templates) {
        const templateRegex = (new RegExp(`{{${template}}}`, 'g'));

        if (!templateRegex.test(viewData))
            continue;

        const templateData = fs.readFileSync(`../templates/${template}.template.html`, 'utf8');

        result = result.replace(templateRegex, templateData);
        addedTemplates.push(template);
    }

    return result;
}