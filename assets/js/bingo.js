const bingoCard = document.getElementById('bingo-card');

for (const child of bingoCard.childNodes) {
    /**
     * @type {HTMLDivElement}
     */
    const element = child;

    element.addEventListener('click', () => {
        if (element.classList.contains('selected'))
            element.classList.remove('selected');
        else
            element.classList.add('selected');
    });
}