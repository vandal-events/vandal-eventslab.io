const slidesElement = document.getElementById('slides');
const fader = document.getElementById('main-fader');

let response = null;

function loadSlides() {
    if (this.responseText)
        response = JSON.parse(this.responseText);

    for (const child of slidesElement.children) {
        slidesElement.removeChild(child);
    }

    const eventToUse = response.events[Math.floor(Math.random() * response.events.length)];
    const imageToUse = eventToUse.images[Math.floor(Math.random() * eventToUse.images.length)];

    /*for (const event of slideData.events) {
        for (const slide of event.images) {
            const slideDiv = document.createElement('div');
            slideDiv.classList.add('slide');

            const slideImg = document.createElement('img');
            slideImg.src = `/img/screenshots/${event.id}/${slide.url}`;
        
            const attribs = document.createElement('span');
            attribs.classList.add('attributions');
            attribs.innerHTML = `Screenshot taken during the <a href="/events/winter2021">${event.name}</a> event.`;

            if (slide.shader) {
                const shaderInfo = slideData.shaders[slide.shader];

                attribs.innerHTML += `<br>Shaders used: <a href="${shaderInfo.url}">${shaderInfo.name}</a>`;
            }

            if (slide.resources) {
                const resourcesInfo = slideData.resources[slide.resources];

                attribs.innerHTML += `<br>Additional resource pack used: <a href="${resourcesInfo.url}">${resourcesInfo.name}</a>`;
            }

            slideDiv.appendChild(slideImg);
            slideDiv.appendChild(attribs);

            slidesElement.appendChild(slideDiv);
        }
    }*/

    const slideDiv = document.createElement('div');
    slideDiv.classList.add('slide');

    const slideImg = document.createElement('img');
    slideImg.src = `/img/screenshots/${eventToUse.id}/${imageToUse.url}`;
        
    const attribs = document.createElement('span');
    attribs.classList.add('attributions');
    attribs.innerHTML = `Screenshot taken during the <a href="/events/winter2021">${eventToUse.name}</a> event.`;

    if (imageToUse.shader) {
        const shaderInfo = response.shaders[imageToUse.shader];

        attribs.innerHTML += `<br>Shaders used: <a href="${shaderInfo.url}">${shaderInfo.name}</a>`;
    }

    if (imageToUse.resources) {
        const resourcesInfo = response.resources[imageToUse.resources];

        attribs.innerHTML += `<br>Additional resource pack used: <a href="${resourcesInfo.url}">${resourcesInfo.name}</a>`;
    }

    slideDiv.appendChild(slideImg);
    slideDiv.appendChild(attribs);

    slidesElement.appendChild(slideDiv);

    fader.style.animationPlayState = 'running';
}

const slideReq = new XMLHttpRequest();
slideReq.addEventListener('load', loadSlides);
slideReq.open('GET', `${location.protocol}/img/screenshots/metadata.json`);
slideReq.send();