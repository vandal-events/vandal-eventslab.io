//5:30 PM CDT
//30th March 2022

const event = {
    name: 'Spring 2022',
    date: 'March 30, 2022 17:30:00',
    url: "/img/previews/spring/spring-1.png"
};

const eventDate = new Date(event.date + ' GMT-0500');

function pad(number) {
    if (number < 0)
        return '00';

    return number > 9 ? number : '0' + number;
}

document.getElementById('image').style.backgroundImage = `url(${event.url})`;

document.getElementById('event-name').innerText = event.name;
document.getElementById('formatted-event').innerText = eventDate.toLocaleString('en-us', { timeZone: 'America/Chicago' });
document.getElementById('formatted-user').innerText = eventDate.toLocaleString('en-us');

setInterval(async () => {
    const now = new Date();
    const diff = eventDate - now;
    const days = Math.floor(diff / (1000 * 60 * 60 * 24));
    const hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    const minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
    const seconds = Math.floor((diff % (1000 * 60)) / 1000);

    const daysStr = pad(days);
    const hoursStr = pad(hours);
    const minutesStr = pad(minutes);
    const secondsStr = pad(seconds);

    const countdown = `${daysStr}:${hoursStr}:${minutesStr}:${secondsStr}`;

    document.getElementById('time').innerHTML = countdown;
}, 250);