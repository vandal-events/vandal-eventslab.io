// https://stackoverflow.com/a/35960429/17841246

const changeFavicon = link => {
    let $favicon = document.querySelector('link[rel="icon"]');
    if ($favicon !== null) {
        $favicon.href = link;
    } else {
        $favicon = document.createElement("link");
        $favicon.rel = "icon";
        $favicon.href = link;
        document.head.appendChild($favicon);
    }
}

const NEXT_VANDAL_EVENT = 'spring';

function getGeppyImage(ext = 'png') {
    return `/img/icons/seasonal/geppy_${NEXT_VANDAL_EVENT}.${ext}`;
}

changeFavicon(getGeppyImage('ico'));

// Update Geppy automatically
const geppyInterval = setInterval(function() {
    try {
        if (document.getElementById('geppy')) {
            document.getElementById('geppy').src = getGeppyImage();
            clearInterval(geppyInterval);
        }

        if (document.getElementsByClassName('geppy')) {
            for (const geppy of document.getElementsByClassName('geppy')) {
                geppy.src = getGeppyImage();
            }

            clearInterval(geppyInterval);
        }
    } catch (_) {}
}, 50);