# This is the official repository for the [Vandal Events website](https://vandal.events).

If you experience any issues, please [report it](https://vandal.events/issues).

# Usage (for developers)
The source code is released under the incredibly permissive [Unlicense](https://choosealicense.com/licenses/unlicense/), so you may use this code for anything you wish without any credit required. However, you may find that you require extra steps to use it.

I very heavily recommend you use GitLab for hosting this, unless you can find a way to get a Node.js system working on GitHub for GitHub Pages, or if you are able to understand how it works enough to make it work on your own webserver.

For testing, you have to run `test/index.js` using Node.js 16. There is a custom templating system that should work in somewhat real-time within the code. It is possible to just run this outright on your own server for production, but I wouldn't recommend it as it may be too slow for that purpose.

Then, you can just go to http://localhost, and the website should load.