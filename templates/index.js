// Custom template system for vandal.events
// I made this because I was too lazy to constantly change the same thing across everything

const fs = require('fs');
const path = require('path');

const templates = fs.readdirSync('./templates')
    .filter(a => a.endsWith('.template.html')).map(a => path.parse(a).name.replace(/.template/g, ''));
const regex = /{{(\w+)}}/g;
const pathRegex = /\S+(?=(\\|\/)views)(\\|\/)/g;

const redirects = [ '/discord', '/twitter', '/gitlab', '/issues' ];

if (!fs.existsSync('../.public'))
    fs.mkdirSync('../.public');

const {readdir} = fs.promises;
const {resolve} = path;
    
async function* getFiles(dir) {
    const dirents = await readdir(dir, { withFileTypes: true });

    for (const dirent of dirents) {
        const res = resolve(dir, dirent.name);
        if (dirent.isDirectory()) {
            yield* getFiles(res);
        } else {
            yield res;
        }
    }
}

function formProperParsed(dir) {
    return dir.replace(pathRegex, '').replace(/\\/g, '/');
}

(async () => {
    for await (const view of getFiles('./views')) {
        // Ignore redirects as a whole // apparently GitLab doesn't support domain-level redirects, disabled for now
        /*if (redirects.some(a => path.parse(view).dir.endsWith(a)))
            continue;*/

        const viewData = fs.readFileSync(view, 'utf8');
        let result = viewData;

        if (!regex.test(viewData)) {
            write(formProperParsed(view), result)

            continue;
        }

        let addedTemplates = [];

        for (const template of templates) {
            const templateRegex = (new RegExp(`{{${template}}}`, 'g'));

            if (!templateRegex.test(viewData))
                continue;

            const templateData = fs.readFileSync(`./templates/${template}.template.html`, 'utf8');

            result = result.replace(templateRegex, templateData);
            addedTemplates.push(template);
        }

        result = `<!-- Generated with templates ${addedTemplates.join(', ')} -->\n<!-- If the source code looks very messy, it's because we don't use frameworks for this, and instead a custom templating system. -->\n${result}`;

        const viewName = formProperParsed(view);

        write(viewName, result)

        console.log(`Added templates "${addedTemplates.join(', ')}" to view ${viewName}`);
    }

    console.log(`Completed update.`);
})();

function write(viewName, result) {
    if (!fs.existsSync(`./.public/${path.parse(viewName).dir}`))
        fs.mkdirSync(`./.public/${path.parse(viewName).dir}`, { recursive: true });

    fs.writeFileSync(`./.public/${viewName}`, result, 'utf8');
}